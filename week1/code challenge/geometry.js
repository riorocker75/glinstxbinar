/*
    rumus tabung 
    V = π x r x r x t
*/
function tabung(r, t) {
  const phi = 3.14;
  return phi * r * r * t;
}

// console.log(tabung(3,5))

/* 
    rumus diameter lingkaran
	K = π × d = 2 × π × r
*/

function d_lingkaran(r) {
  return 2 * r;
}
