const array = [11, 15, 20, 26, 23, 2345, 113, 5523];

function bubbleSort(array) {
  const jumlahArray = array.length;

  let terdeteksiPerubahan = false;

  for (let i = 0; i < jumlahArray; i++) {
    for (let j = 0; j < jumlahArray - i - 1; j++) {
      if (array[j] > array[j + 1]) {
        // swap the elements
        const temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;

        terdeteksiPerubahan = true;
      }
    }

    if (!terdeteksiPerubahan) {
      break;
    }
  }

  return array;
}

const filteredArray = array.filter((item) => item != null);

console.log(bubbleSort(filteredArray));
