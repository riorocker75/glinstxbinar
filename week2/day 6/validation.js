/* If we use callback function, we can't add another logic outside the function */

// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function to calculate tabung volume
function tabung(jari,tinggi) {
      const phi = 3.14;
  return phi * jari * jari * tinggi;
}

/* Way 1 */
// Function for inputing length of tabung
function inputJari() {
  rl.question(`Jari-jari: `, (jari) => {
    if (!isNaN(jari)) {
      inputTinggi(jari);
    } else {
      console.log(`Jari-jari must be a number\n`);
      inputJari();
    }
  });
}

// Function for inputing width of tabung
function inputTinggi(jari) {
  rl.question(`Tinggi: `, (tinggi) => {
    if (!isNaN(tinggi)) {
       console.log(`\ntabung: ${tabung(jari, tinggi)}`);
       rl.close();
    } else {
      console.log(`Tinggi must be a number\n`);
      inputTinggi(jari);
    }
  });
}

// Function for inputing height of tabung
// function inputHeight(length, width) {
//   rl.question(`Height: `, (height) => {
//     if (!isNaN(height)) {
//       console.log(`\ntabung: ${tabung(length, width, height)}`);
//       rl.close();
//     } else {
//       console.log(`Height must be a number\n`);
//       inputHeight(length, width);
//     }
//   });
// }
/* End Way 1 */

/* Alternative Way */
// All input just in one code
function input() {
  rl.question("Jari-jari: ", function (jari) {
    rl.question("Tinggi: ", (tinggi) => {
        if (jari > 0 && tinggi > 0 ) {
          console.log(`\nHasil Rumus Tabung: ${tabung(tinggi, jari)}`);
          rl.close();
        } else {
          console.log(`jari-jari, tinggi must be a number\n`);
          input();
        }
    
    });
  });
}
/* End Alternative Way */

console.log(`Rumus Tabung`);
console.log(`=========`);
inputJari(); // Call way 1
// input(); // Call Alternative Way
